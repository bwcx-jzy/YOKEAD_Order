package com.yokead.system.init;//package com.yokead.system.init;
//
//import cn.jiangzeyin.common.PreLoadClass;
//import com.yokead.nginxlog.ReadLogJob;
//import com.yokead.system.log.SystemLog;
//import org.quartz.*;
//import org.quartz.impl.StdSchedulerFactory;
//
///**
// * Created by jiangzeyin on 2018/5/9.
// */
//@PreLoadClass
//public class log {
//    private volatile static Scheduler scheduler = null;
//    private static final StdSchedulerFactory STD_SCHEDULER_FACTORY = new StdSchedulerFactory();
//
//    private static void init() {
//        JobDetail job = JobBuilder.newJob(ReadLogJob.class)
//                .withIdentity("adLog", "nginx")
//                .build();
//        CronTrigger trigger = TriggerBuilder.newTrigger()
//                .withIdentity("adLog", "nginx")
//                .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * * * ?")).build();
//        try {
//            getScheduler().scheduleJob(job, trigger);
//            // 开启调度
//            getScheduler().start();
//        } catch (SchedulerException e) {
//            SystemLog.ERROR().error("启动任务失败", e);
//        }
//        SystemLog.LOG().info("启动日志分析成功");
//    }
//
//    /**
//     * @return
//     * @throws SchedulerException
//     * @author jiangzeyin
//     * @date 2016-9-1
//     */
//    private static Scheduler getScheduler() throws SchedulerException {
//        // 通过schedulerFactory获取一个调度器
//        if (scheduler == null) {
//            synchronized (log.class) {
//                scheduler = STD_SCHEDULER_FACTORY.getScheduler();
//            }
//        }
//        return scheduler;
//    }
//}
