package com.yokead.controller.domain;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.domain.DoMainServer;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by jiangzeyin on 2017/5/10.
 */
@Controller
@RequestMapping("domain")
public class EditDoMainControl extends AdminBaseControl {
    @Resource
    private DoMainServer doMainServer;

    @RequestMapping(value = "edit_domain.html", produces = MediaType.TEXT_HTML_VALUE)
    public String edit_domain(String domain, String name) {
        try {
            List<String> logs = doMainServer.domain_log(domain, name);
            setAttribute("logs", logs);
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("页面编辑一次", e);
        }
        String ip = Config.Admin.getDefaultIp();
        if (!StringUtil.isEmpty(ip))
            setAttribute("ip", ip);
        return "domain/edit_domain";
    }


    @RequestMapping(value = "add_domain.html", produces = MediaType.TEXT_HTML_VALUE)
    public String add_domain() {
        setAttribute("ip", Config.Admin.getDefaultIp());
        return "domain/create_domain";
    }

    @RequestMapping(value = "update_domain.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String update_domain(String domain, String id, String ip) {
        int code = doMainServer.update_domain_(domain, id, ip);
        if (code != 0 && code != 1) {
            return JsonMessage.getString(100, "修改失败，" + code);
        }
        return JsonMessage.getString(200, "修改成功");
    }


    @RequestMapping(value = "create_domain.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String create_domain(String domain, String name, String ip) {
        int code = doMainServer.create_domain_(domain, name, ip);
        if (code == 1)
            return JsonMessage.getString(200, "创建成功");
        return JsonMessage.getString(100, "创建失败：" + code);
    }


}
