package com.yokead.controller.domain;

import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.tools.AllUrl;
import com.yokead.service.domain.DoMainServer;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/10.
 */
@Controller
@RequestMapping("domain")
public class DoMainControl extends AdminBaseControl {

    @Resource
    private DoMainServer doMainServer;

    @RequestMapping(value = "index.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index() throws IOException {
        //JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject = doMainServer.getUrls();
        JSONObject default_ = jsonObject.getJSONObject("default");
        if (default_ != null) {
            setAttribute("company", default_.getJSONArray("company"));
            setAttribute("personal", default_.getJSONArray("personal"));
        }
        JSONObject customer = jsonObject.getJSONObject("customer");
        if (customer != null) {
            setAttribute("kh", customer.getJSONArray(userName));
        }
        //List<String> personal = doMainServer.getUrls(getBootPath() + "/domain_list.conf", "personal");

        return "domain/index";
    }

    @RequestMapping(value = "get_child.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String get_child(String name) {
        return getChild(name).toString();
    }


    public JsonMessage getChild(String name) {
        try {
            JSONArray jsonArray = doMainServer.getDomainRList(name);
            if (jsonArray == null) {
                return new JsonMessage(400, "域名信息错误：没有对应信息");
            }
            return new JsonMessage(200, "", jsonArray);
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("域名处理错误", e);
            return new JsonMessage(500, "域名信息获取错误，请联系管理员");
        }
    }

    @RequestMapping(value = "get_domain_child.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String d(String name) {
        try {
            JSONArray jsonArray = AllUrl.start(Config.Nginx.getNginxConfigPath(), name);
            return JsonMessage.getString(200, "获取成功", jsonArray);
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取页面失败", e);
            return JsonMessage.getString(100, "获取失败");
        }
    }
}
