package com.yokead.controller.order;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.order.ProductService;
import com.yokead.util.JsonUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.File;
import java.util.UUID;

/**
 * Created by jiangzeyin on 2017/6/22.
 */
@Controller
@RequestMapping("order")
public class OperControl extends AdminBaseControl {
    @Resource
    private ProductService productService;


    @RequestMapping(value = "edit_oper.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String create(String name, String productName) throws Exception {
        JSONArray jsonArray = productService.getUserProductJson(name);
        if (jsonArray != null) {
            JSONObject jsonObject = productService.getJSONObjectName(productName, jsonArray);
            if (jsonObject != null) {
                JSONObject object = jsonObject.getJSONObject("oper");
                if (object != null) {
                    String key = object.getString("key");
                    if (!cn.jiangzeyin.StringUtil.isEmpty(key))
                        setAttribute("key", key);
                }
            }
        }
        setAttribute("prefix", Config.Order.getOperUrlPrefix());
        setAttribute("name", name);
        setAttribute("productName", productName);
        return "order/edit_oper";
    }

    @RequestMapping(value = "save_oper.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String save_oper(String name, String productName, String value) throws Exception {
        name = convertFilePath(name);
        productName = convertFilePath(productName);
        if (StringUtil.isEmpty(value, 6, 20))
            return JsonMessage.getString(400, "请输入6-20的位的密码");
        JSONArray jsonArray = productService.getUserProductJson(name);
        if (jsonArray == null)
            return JsonMessage.getString(100, "没有对应信息");
        JSONObject jsonObject = productService.getJSONObjectName(productName, jsonArray);
        if (jsonObject == null)
            return JsonMessage.getString(100, "没有对应产品信息");
        JSONObject object = jsonObject.getJSONObject("oper");
        if (object == null) {
            object = new JSONObject();
            object.put("key", UUID.randomUUID());
            jsonObject.put("oper", object);
        }
        object.put("pwd", value);
        //System.out.println(jsonArray);
        //String newsJson = JsonUtil.formatJson(jsonArray.toString());
        File file = productService.getUserProductFile(name);
        //FileUtil.writeFile(file.getPath(), newsJson);
        JsonUtil.saveJson(file.getPath(), jsonArray);
        return JsonMessage.getString(200, "修改成功");
    }

}
