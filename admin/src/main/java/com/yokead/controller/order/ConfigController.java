package com.yokead.controller.order;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.BaseService;
import com.yokead.service.order.ProductService;
import com.yokead.util.JsonUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2018/7/29.
 */
@Controller
@RequestMapping("order")
public class ConfigController extends AdminBaseControl {

    @Resource
    private ProductService productService;

    /**
     * 配置
     *
     * @return name
     */
    @RequestMapping(value = "edit_config.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String edit(String name, String id) throws IOException {
        JSONArray jsonArray1 = getData(name, id);
        JSONArray config = ProductService.getProductLimitDuplicateConfig(name, id);
        JSONObject dataConfig = new JSONObject();
        if (config != null && jsonArray1 != null) {
            JSONObject dataOptConfig = null;
            int len = config.size();
            for (int i = len - 1; i >= 0; i--) {
                JSONObject jsonObject = config.getJSONObject(i);
                String kName = jsonObject.getString("name");
                if (StringUtil.isEmpty(kName)) {
                    if (i == len - 1)
                        dataOptConfig = jsonObject;
                    continue;
                }
                int limit = jsonObject.getIntValue("limit");
                dataConfig.put(kName, limit);
            }
            setAttribute("optConfig", dataOptConfig);
        }
        setAttribute("data", dataConfig);
        setAttribute("array", jsonArray1);
        return "order/edit_config";
    }

    private JSONArray getData(String name, String id) throws IOException {
        JSONArray jsonArray = productService.getUserProductJson(name);
        if (jsonArray == null)
            throw new RuntimeException("没有找产品:" + name + "  " + id);
        JSONObject jsonObject = productService.getJSONObjectName(id, jsonArray);
        if (jsonObject == null)
            throw new RuntimeException("没有找产品");
        String orderType = jsonObject.getString("orderType");
        return BaseService.getOrderColumnByType(orderType);
    }


    @RequestMapping(value = "save_config.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String save_oper(String adminPId, String adminPname) throws IOException {
        JSONArray config = new JSONArray();
        JSONArray jsonArray1 = getData(adminPname, adminPId);
        for (int i = jsonArray1.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = jsonArray1.getJSONObject(i);
            String pName = jsonObject.getString("name");
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("name", pName);
            jsonObject1.put("limit", getParameterInt(pName));
            config.add(jsonObject1);
        }
        JSONObject dataConfig = new JSONObject();
        String configDataIntercept = getParameter("configDataIntercept");
        String configMaskTip = getParameter("configMaskTip");
        dataConfig.put("dataIntercept", "on".equalsIgnoreCase(configDataIntercept));
        dataConfig.put("maskTip", "on".equalsIgnoreCase(configMaskTip));
        config.add(dataConfig);
        File file = productService.getProductLimitDuplicateJsonFile(adminPname, adminPId);
        JsonUtil.saveJson(file.getPath(), config);
        return JsonMessage.getString(200, "保存成功");
    }
}
