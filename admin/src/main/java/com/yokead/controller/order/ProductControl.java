package com.yokead.controller.order;

import com.alibaba.fastjson.JSONArray;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.order.OrderService;
import com.yokead.service.order.ProductService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/15.
 */
@Controller
@RequestMapping("order")
public class ProductControl extends AdminBaseControl {

    @Resource
    private ProductService productService;
    @Resource
    private OrderService orderService;

    @RequestMapping(value = "product.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index(String name) throws Exception {
        name = convertFilePath(name);
        JSONArray jsonArray = productService.getUserAll(name);
        setAttribute("array", jsonArray);
        setAttribute("khName", name);
        //String url = SpringUtil.getEnvironment().getProperty("order.url");
        //setAttribute("url", url);
        return "order/product";
    }


    @RequestMapping(value = "select_type.html", produces = MediaType.TEXT_HTML_VALUE)
    public String select_type() throws IOException {
        JSONArray jsonArray = orderService.getOrderType();
        setAttribute("array", jsonArray);
        return "order/select_order_type";
    }
}
