package com.yokead.controller.api;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.interceptor.NotLogin;
import com.yokead.common.tools.UrlPath;
import com.yokead.controller.copy.CopyIndexControl;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import com.yokead.util.EncryptUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by jiangzeyin on 2017/9/26.
 */
@Controller
@RequestMapping("api")
public class CopyListControl extends AdminBaseControl {

    @RequestMapping(value = "copylist.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @NotLogin
    public String copylist(String url) {
        if (StringUtil.isEmpty(url))
            return JsonMessage.getString(400, "url 错误");
        JSONObject jsonObject;
        try {
            jsonObject = CopyIndexControl.getInfo(UrlPath.Type.None, null, Config.Nginx.getNginxConfigPath());
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取失败", e);
            return JsonMessage.getString(100, "获取系统异常");
        }
        if (jsonObject == null)
            return JsonMessage.getString(101, "没有获取到数据");
        Collection<Object> collection = jsonObject.values();
        JSONArray urls = new JSONArray();
        for (Object object : collection) {
            JSONArray jsonArray = (JSONArray) object;
            for (Object item : jsonArray) {
                JSONObject jsonObject1 = (JSONObject) item;
                String editPath = jsonObject1.getString("editPath");
                String canUrl = "http://" + jsonObject1.getString("url");
                File file;
                // 解密
                try {
                    String path = EncryptUtil.decrypt(editPath);
                    file = new File(path);
                    if (!file.exists())
                        continue;
                } catch (Exception e) {
                    SystemLog.ERROR().error("解密异常", e);
                    continue;
                }
                // 转换
                Document document;
                try {
                    document = Jsoup.parse(file, "UTF-8");
                } catch (IOException e) {
                    SystemLog.ERROR().error("转换异常", e);
                    continue;
                }
                Elements iframes = document.getElementsByTag("iframe");
                iframes.forEach(element -> {
                    String src = element.attr("src");
                    if (url.equals(src)) {
                        urls.add(canUrl);
                    }
                });
            }
        }
        if (urls.size() < 1)
            return JsonMessage.getString(404, "没有数据");
        return JsonMessage.getString(200, "ok", urls);
    }
}
