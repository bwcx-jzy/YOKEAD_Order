package com.yokead.controller.rewrite;

import com.yokead.common.base.RewriteBaseControl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 广告跳转系统。第二版
 *
 * @author yangheng
 */
@Controller
@RequestMapping(value = "/rewrite-2")
public class RewriteController extends RewriteBaseControl {

    @RequestMapping(value = "/index.html")
    public String index() {
        return "rewrite/index";
    }


}
