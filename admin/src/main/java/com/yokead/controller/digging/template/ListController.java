package com.yokead.controller.digging.template;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.jiangzeyin.StringUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.tools.UrlPath;
import com.yokead.controller.copy.CopyIndexControl;
import com.yokead.service.template.TemplateService;
import com.yokead.util.JsonUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by jiangzeyin on 2018/5/11.
 */
@Controller
@RequestMapping("digging/template")
public class ListController extends AdminBaseControl {

    @Resource
    private TemplateService templateService;

    @RequestMapping(value = "index.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index(String name) throws IOException {
        JSONObject data = CopyIndexControl.getInfo(UrlPath.Type.PageTemplate, name, Config.Nginx.getNginxConfigPath());
        setAttribute("data", data);
        return "digging/template/index";
    }

    @RequestMapping(value = "create.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String create(String tag) throws IOException {
        if (StringUtil.isEmpty(tag)) {
            return "digging/template/create";
        }
        JSONArray jsonArray = templateService.getTemplateField(tag);
        setAttribute("array", jsonArray);
        return "digging/template/create";
    }

    @RequestMapping(value = "select.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String select() throws IOException {
        File file = Config.Admin.getPageTemplatePath();
        file = new File(file, "config.json");
        if (!file.exists() && !file.isFile()) {
            throw new IllegalArgumentException("没有配置相关信息");
        }
        JSONArray jsonArray = (JSONArray) JsonUtil.readJson(file.getPath());
        setAttribute("array", jsonArray);
        return "digging/template/select";
    }

    @RequestMapping(value = "preview.html", method = RequestMethod.GET)
    @ResponseBody
    public void preview(String path) throws IOException {
        PrintWriter printWriter = getResponse().getWriter();
        path = convertFilePath(path);
        if (StringUtil.isEmpty(path)) {
            printWriter.write("path error");
            printWriter.flush();
            return;
        }
        File file = Config.Admin.getPageTemplatePath();
        file = new File(file, path);
        if (!file.exists() && !file.isFile()) {
            printWriter.write("file error");
            printWriter.flush();
            return;
        }
        getResponse().setContentType("text/html;charset=UTF-8");
        getResponse().setCharacterEncoding("UTF-8");
        printWriter.write(FileUtil.readString(file, CharsetUtil.CHARSET_UTF_8));
        printWriter.flush();
    }
}
