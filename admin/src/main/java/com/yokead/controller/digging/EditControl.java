package com.yokead.controller.digging;//package com.yoke.controller.digging;
//
//import com.yokead.common.base.DiggingBaseControl;
//
//import org.jsoup.Connection;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.select.Elements;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
///**
// * Created by jiangzeyin on 2017/6/30.
// */
//@Controller
//@RequestMapping("digging")
//public class EditControl extends DiggingBaseControl {
//    @RequestMapping(value = "edit_1.html", produces = MediaType.TEXT_HTML_VALUE)
//    public String edit(String url) {
//        try {
//            Connection connection = Jsoup.connect(url);
//            Document document = connection.timeout(10 * 1000).get();
//            Elements scripts = document.getElementsByTag("script");
//            convertUrl(scripts, url, "src");
//            Elements links = document.getElementsByTag("link");
//            convertUrl(links, url, "href");
//            Elements imgS = document.getElementsByTag("img");
//            convertUrl(imgS, url, "src");
//            {
//                Elements styles = document.getElementsByAttributeValueMatching("style", "display:none;");
//                styles.remove();
//            }
//            {
//                Elements styles = document.getElementsByAttributeValueMatching("style", "display:none");
//                styles.remove();
//            }
//            request.setAttribute("html", StringUtil.filterHTML(document.toString()));
//            //return document.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//            // return StringUtil.fromException(e);
//        }
//        return "digging/edit_1";
//    }
//}
