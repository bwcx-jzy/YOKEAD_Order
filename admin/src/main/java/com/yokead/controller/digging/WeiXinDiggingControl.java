package com.yokead.controller.digging;//package com.yoke.controller.digging;
//
//import com.yokead.common.base.AdminBaseControl;
//
//import org.jsoup.Connection;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.select.Elements;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
///**
// * Created by jiangzeyin on 2017/6/28.
// */
//@Controller
//@RequestMapping("digging")
//public class WeiXinDiggingControl extends AdminBaseControl {
//
//    static String imgUrl = "http://read.html5.qq.com/image?src=forum&q=5&r=0&imgflag=7&imageUrl=";
//
//    @RequestMapping(value = "weixin1.html", produces = MediaType.TEXT_HTML_VALUE + ";charset=UTF-8")
//    @ResponseBody
//    public String weixin1(String url) {
//        //response.setCharacterEncoding("UTF-8");
//        if (StringUtil.isEmpty(url))
//            return "请填写正确的url";
//        try {
//            Connection connection = Jsoup.connect(url);
//            Document document = connection.get();
//            Elements scripts = document.getElementsByTag("script");
//            scripts.remove();
//            Elements imgs = document.getElementsByTag("img");
//            imgs.forEach(o -> {
//                o.attr("src", imgUrl + o.attr("data-src"));
//                o.removeAttr("data-src");
//            });
//            {
//                Elements styles = document.getElementsByAttributeValueMatching("style", "display:none;");
//                styles.remove();
//            }
//            {
//                Elements styles = document.getElementsByAttributeValueMatching("style", "display:none");
//                styles.remove();
//            }
//            Elements metaS = document.getElementsByTag("meta");
//            metaS.last().after("<meta name=\"referrer\" content=\"never\">");
//            {
//                Elements bgS = document.getElementsByAttributeValue("class", "__bg_gif");
//                bgS.forEach(o -> {
//                    String style = o.attr("style");
//                    String[] styleS = StringUtil.stringToArray(style, ";");
//                    if (styleS != null) {
//                        StringBuilder stringBuffer = new StringBuilder();
//                        for (String item : styleS) {
//                            if (stringBuffer.length() > 0)
//                                stringBuffer.append(";");
//                            item = item.trim();
//                            if (item.startsWith("background-image")) {
//                                String urlL = item.substring(item.indexOf("(") + 1, item.indexOf(")"));
//                                stringBuffer.append("background-image:url(");
//                                stringBuffer.append(imgUrl);
//                                stringBuffer.append(urlL).append(")");
//                            } else {
//                                stringBuffer.append(item);
//                            }
//                        }
//                        o.attr("style", stringBuffer.toString());
//                    }
//                });
//            }
//            return document.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return StringUtil.fromException(e);
//        }
//    }
//}
