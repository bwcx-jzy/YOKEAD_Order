package com.yokead.controller.digging;//package com.yoke.controller.digging;
//
//import com.yokead.common.base.DiggingBaseControl;
//import com.yokead.common.base.AdminBaseControl;
//import com.yokead.util.EncodeFileUtil;
//
//import org.jsoup.Connection;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.select.Elements;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
///**
// * Created by jiangzeyin on 2017/6/28.
// */
//@Controller
//@RequestMapping("digging")
//public class PtControl extends DiggingBaseControl {
//
//    @RequestMapping(value = "f_weixin_1.html", produces = MediaType.TEXT_HTML_VALUE + ";charset=UTF-8")
//    @ResponseBody
//    public String f_weixin_1(String url, String js) {
//        if (StringUtil.isEmpty(url))
//            return "请填写正确的url";
//        try {
//            Connection connection = Jsoup.connect(url);
//            Document document = connection.timeout(10 * 1000).get();
//            Boolean removeJs = Boolean.valueOf(js);
//            Elements scripts = document.getElementsByTag("script");
//            if (removeJs) {
//                scripts.remove();
//            } else {
//                convertUrl(scripts, url, "src");
//            }
//            Elements links = document.getElementsByTag("link");
//            convertUrl(links, url, "href");
//            Elements imgS = document.getElementsByTag("img");
//            convertUrl(imgS, url, "src");
//            {
//                Elements styles = document.getElementsByAttributeValueMatching("style", "display:none;");
//                styles.remove();
//            }
//            {
//                Elements styles = document.getElementsByAttributeValueMatching("style", "display:none");
//                styles.remove();
//            }
//            return document.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return StringUtil.fromException(e);
//        }
//    }
//
//}
