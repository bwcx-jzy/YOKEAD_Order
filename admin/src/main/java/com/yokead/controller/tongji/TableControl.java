package com.yokead.controller.tongji;

import cn.jiangzeyin.DateUtil;
import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import cn.simplifydb.database.base.BaseRead;
import cn.simplifydb.database.run.read.SelectPage;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.base.AdminBaseControl;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by jiangzeyin on 2017/8/11.
 */
@Controller
@RequestMapping("tongji")
public class TableControl extends AdminBaseControl {

    @RequestMapping(value = "table.html", produces = MediaType.TEXT_HTML_VALUE)
    public String details(String url) {
        return "tongji/table";
    }

    @RequestMapping(value = "table_data.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String table_data(String url, String startTime, String endTime, String pageNo) throws IOException, ParseException {
        if (StringUtil.isEmpty(url))
            return JsonMessage.getString(100, "链接不正确");
        if (!url.startsWith("http://"))
            return JsonMessage.getString(100, "链接不正确,不是有效的http");
        String tempUrl = url.substring(url.indexOf("http://") + 7);
        int s = tempUrl.indexOf("/");
        if (s <= -1)
            return JsonMessage.getString(100, "链接不正确,-1");
        int e = tempUrl.lastIndexOf("/");
        if (s < e) {
            url = url.substring(0, e + 1 + 7);
        }
        int startTime_int = 0;
        if (!StringUtil.isEmpty(startTime)) {
            startTime = startTime + " 00:00:00";
            startTime_int = (int) (DateUtil.parseTime(startTime, "yyyy-MM-dd HH:mm:ss").getTime() / 1000L);
            if (startTime_int <= 0)
                return JsonMessage.getString(140, "查询的开始时间不正确:" + startTime);
        }
        int endTime_int = 0;
        if (!StringUtil.isEmpty(endTime)) {
            endTime += " 23:59:59";
            endTime_int = (int) (DateUtil.parseTime(endTime, "yyyy-MM-dd HH:mm:ss").getTime() / 1000L);
            if (endTime_int <= 0)
                return JsonMessage.getString(140, "查询的结束时间不正确：" + endTime);
        }
        return doSelect(url, pageNo, startTime_int, endTime_int);
    }

    private String doSelect(String url, String pageNo, int startTime, int endTIme) {
        int pageNo_int = StringUtil.parseInt(pageNo, 1);
        int pageSize_int = 10;
        SelectPage<?> selectPage = new SelectPage<>();
        selectPage.setTag("core");
        selectPage.setSql("select * from nginxAdLog");

//        Page<?> page = new Page();
        selectPage.setPageNoAndSize(pageNo_int, pageSize_int);
        selectPage.where("url LIKE '" + url + "%'");
        if (startTime > 0) {
            selectPage.whereAnd("time>=" + startTime);
        }
        if (endTIme > 0) {
            selectPage.whereAnd("time<=" + endTIme);
        }
        selectPage.orderBy("time desc");
//        try {
//            SqlUtil.doPage(page);
//        } catch (SQLException e) {
//            SystemLog.ERROR().error("分页错误", e);
//            return JsonMessage.getString(500, "系统异常");
//        }
//        SelectPage<?> selectPag = new SelectPage<>(page);
//        selectPag.setTag("core");
        selectPage.setResultType(BaseRead.Result.PageResultType);
        JSONObject result = selectPage.run();
        return JsonMessage.getString(200, "获取成功", result);
    }
}
