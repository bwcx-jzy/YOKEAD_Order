package com.yokead.controller.tongji;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.tools.UrlPath;
import com.yokead.controller.copy.CopyIndexControl;
import com.yokead.service.nginxlog.StatisticsService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URL;

/**
 * 统计
 *
 * @author yangheng
 */
@Controller
@RequestMapping("/statistics")
public class StatisticsController extends AdminBaseControl {

    @Resource
    private StatisticsService statisticsService;

    /**
     * 统计功能首页
     *
     * @param name 名称
     * @return String
     * @throws IOException ex
     */
    @RequestMapping(value = "index.html", produces = MediaType.TEXT_HTML_VALUE)
    public String index(String name) throws IOException {
        setAttribute("data", CopyIndexControl.getInfo(UrlPath.Type.None, name, Config.Nginx.getNginxConfigPath()));
        return "tongji/index";
    }

    /**
     * 查看详情页面
     *
     * @return String
     */
    @RequestMapping(value = "/detail.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String detail() {
        return "tongji/details2";
    }


    /**
     * 查看某个链接的详情信息
     *
     * @param urlStr 链接地址
     * @return String
     */
    @RequestMapping(value = "/detail.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String detail(@RequestParam(name = "url", required = false) String urlStr
            , @RequestParam(name = "search", required = false) String search, Long startTime, Long endTime
    ) {
        if (null == startTime || null == endTime || startTime > endTime) {
            return JsonMessage.getString(500, "获取时间范围失败");
        }
        String host = getHostForUrl(urlStr);
        if (StrUtil.isEmpty(host)) {
            return JsonMessage.getString(500, "获取地址失败");
        }
        return statisticsService.chats(host, search, startTime, endTime);
    }


    /**
     * 查看详情页面的表格数据
     *
     * @param urlStr    访问的链接
     * @param search    搜索条件
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param page      第几页
     * @param limit     一页几行
     * @return String
     */
    @RequestMapping(value = "/detail_table.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String detailTable(@RequestParam(name = "url", required = false) String urlStr
            , @RequestParam(name = "search", required = false) String search, Long startTime, Long endTime
            , int page, int limit
    ) {
        String host = getHostForUrl(urlStr);
        if (StrUtil.isEmpty(host)) {
            return JsonMessage.getString(500, "获取地址失败");
        }
        if (null == startTime || null == endTime || startTime > endTime) {
            return JsonMessage.getString(500, "获取时间范围失败");
        }
        return statisticsService.tableData(host, search, startTime, endTime, page, limit);
    }


    private String getHostForUrl(String urlStr) {
        if (!Validator.isUrl(urlStr)) {
            return null;
        }
        URL url = URLUtil.url(urlStr);
        //域名
        return url.getHost();
    }


}
