package com.yokead.controller.tongji;

import com.yokead.service.nginxlog.NginxLog2019Service;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * nginx管理Controller
 *
 * @author yangheng
 */
@Controller
@RequestMapping(value = "/nginx_log")
public class NginxLogController {

    ///建表操作只需要执行一次就行

    /**
     * 创建Nginx_log_2019数据库表结构
     *
     * @return String
     */
    @RequestMapping(value = "/create_table", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String createTable() {
///        DataSource dataSource = DatabaseContextHolder.getDataSource();
//        try {
//            JdbcUtils.execute(dataSource, "drop table `Nginx_Log_2019` ");
//            JdbcUtils.execute(dataSource, Nginx_Log_2019.CREATE_TABLE_SQL);
//            return JsonMessage.getString(200, "创建表结构成功");
//        } catch (SQLException e) {
//            SystemLog.ERROR().error("创建Nginx_2019_表结构失败", e);
//            return JsonMessage.getString(500, "创建失败");
//        }
        return "";
    }


    @Resource
    private NginxLog2019Service nginxLog2019Service;

    @RequestMapping(value = "/insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String insert() {
///        Nginx_Log_2019 item = new Nginx_Log_2019();
//        item.setUrl("test");
//        item.setIp("test");
//        item.setXip("test");
//        item.setUag("test");
//        item.setType("test");
//        item.setTime(0L);
//        item.setStatus(200);
//
//        nginxLog2019Service.insertItem(item);
        return "";
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String list() {
///        return nginxLog2019Service.list();
        return "";
    }
}
