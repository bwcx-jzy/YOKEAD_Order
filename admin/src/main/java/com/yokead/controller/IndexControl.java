package com.yokead.controller;

import com.yokead.common.UserUtil;
import com.yokead.common.base.AdminBaseControl;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;

/**
 * @author jiangzeyin
 * Created by jiangzeyin on 2017/5/10.
 */
@Controller
public class IndexControl extends AdminBaseControl {

    @RequestMapping(value = {"index.html", "", "/"}, method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index() throws IOException {
        // 是否是管理
        setSessionAttribute("manage", UserUtil.isManage(userName));
        return "index";
    }
}
