package com.yokead.controller.copy;

import cn.jiangzeyin.common.JsonMessage;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import com.yokead.util.EncryptUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;

/**
 * Created by jiangzeyin on 2017/7/6.
 */
@Controller
@RequestMapping("copy")
public class DeleteControl extends AdminBaseControl {

    @RequestMapping(value = "delete.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String get_log(String url, String tag) {
        String path;
        try {
            path = EncryptUtil.decrypt(tag);
            File file = new File(path);
            if (!file.exists()) {
                return JsonMessage.getString(404, "不存在该文案");
            }
            FileCopyUtils.copy(file, new File(file.getParentFile(), "index_back.html"));
            if (file.delete()) {
                return JsonMessage.getString(200, "");
            }
            return JsonMessage.getString(404, "删除失败");
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("删除异常", e);
            return JsonMessage.getString(500, "删除异常:" + e.getMessage());
        }
    }
}
