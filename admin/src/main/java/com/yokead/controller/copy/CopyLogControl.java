package com.yokead.controller.copy;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.base.DiggingBaseControl;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import com.yokead.util.EncryptUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;

/**
 * Created by jiangzeyin on 2017/7/5.
 */
@Controller
@RequestMapping("copy")
public class CopyLogControl extends AdminBaseControl {

    @RequestMapping(value = "reply_log.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String get_log(String url, String tag) {
        String path;
        try {
            path = EncryptUtil.decrypt(tag);
            File file = new File(path);
            if (!file.exists()) {
                return JsonMessage.getString(404, "不存在该文案");
            }
            if (StringUtil.isEmpty(url)) {
                return JsonMessage.getString(404, "了解不能为空");
            }
            url = url.substring(url.lastIndexOf('/'));
            url = convertFilePath(url);
            File logFile = new File(file.getParentFile(), url);
            if (!logFile.exists()) {
                return JsonMessage.getString(404, "不存在该日志");
            }
            //.readToString(logFile);
            String olgContext = FileUtil.readString(logFile, CharsetUtil.CHARSET_UTF_8);
            DiggingBaseControl.backIndexHtml(logFile.getParentFile());
            FileUtil.writeString(olgContext, file, CharsetUtil.CHARSET_UTF_8);
            return JsonMessage.getString(200, "");
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("还原异常", e);
            return JsonMessage.getString(500, "还原异常:" + e.getMessage());
        }
    }

    @RequestMapping(value = "get_log.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String get_log(String tag) {
        String path;
        try {
            path = EncryptUtil.decrypt(tag);
            File file = new File(path);
            if (!file.exists()) {
                return JsonMessage.getString(404, "不存在该文案");
            }
            File[] file1 = DiggingBaseControl.listLogs(file.getParentFile());
            if (file1 == null || file1.length < 1) {
                return JsonMessage.getString(400, "没有记录");
            }
            JSONArray jsonArray = new JSONArray();
            for (File item : file1) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", item.getName());
                String time = new DateTime(item.lastModified()).toString(DatePattern.NORM_DATETIME_MS_PATTERN);
                jsonObject.put("time", time);
                jsonArray.add(jsonObject);
            }
            return JsonMessage.getString(200, "", jsonArray);
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
            return JsonMessage.getString(500, "获取异常:" + e.getMessage());
        }
    }
}
