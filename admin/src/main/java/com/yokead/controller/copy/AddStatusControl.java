package com.yokead.controller.copy;

import cn.jiangzeyin.common.JsonMessage;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import com.yokead.util.EncryptUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;

/**
 * Created by jiangzeyin on 2017/7/5.
 */
@Controller
@RequestMapping("copy")
public class AddStatusControl extends AdminBaseControl {
    @RequestMapping(value = "add_status.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String add_status(String status, String tag) {
        String json = checkTag(tag, status);
        if (json == null)
            json = JsonMessage.getString(200, "");
        return json;
    }

    public static String checkTag(String tag, String status) {
        String path;
        try {
            path = EncryptUtil.decrypt(tag);
            File file = new File(path);
            if (!file.exists()) {
                return JsonMessage.getString(400, "tag 不正确");
            }
            if ("tf".equals(status)) {
                File tf = new File(file.getParentFile(), "index_tf.html");
                if (tf.exists()) {
                    return JsonMessage.getString(300, "已经存在投放页啦");
                }
            } else if ("sh".equals(status)) {
                File sh = new File(file.getParentFile(), "index_sh.html");
                if (sh.exists()) {
                    return JsonMessage.getString(300, "已经存在审核页啦");
                }
            } else
                return JsonMessage.getString(400, "status 不正确");
            return null;//JsonMessage.getString(200, "");
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("系统异常", e);
            return JsonMessage.getString(500, "系统异常，" + e.getMessage());
        }
    }
}
