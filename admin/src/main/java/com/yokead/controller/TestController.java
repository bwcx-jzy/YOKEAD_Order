package com.yokead.controller;

import cn.jiangzeyin.common.JsonMessage;
import cn.simplifydb.database.base.BaseRead;
import cn.simplifydb.database.run.read.Select;
import com.alibaba.fastjson.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/test")
public class TestController {

    @RequestMapping(value = "/test")
    @ResponseBody
    public String testTongji() {
        Select<?> select = new Select<>();
        select.setTag("core");
        select.setSql("select count(1)  from nginxAdLog");
        select.orderBy("time desc");
        select.limit(10);
        select.setResultType(BaseRead.Result.JsonArray);
        JSONArray result = select.run();
        return JsonMessage.getString(200, "获取成功", result);
    }

}
