package com.yokead.controller.sysadmin;

import cn.jiangzeyin.common.JsonMessage;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.controller.tongji.TimesUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by jiangzeyin on 2017/8/9.
 */
@Controller
@RequestMapping("sysadmin")
public class TongJiControl extends AdminBaseControl {


    @RequestMapping(value = "tongji.html", produces = MediaType.TEXT_HTML_VALUE)
    public String details(String url) throws IOException {
        return "sysadmin/tongji";
    }

    /**
     * 获取数据详情
     *
     * @param dateType
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "data.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String data(String dateType, String startTime, String endTime, String dime) throws ParseException {
        if ("day".equals(dime)) {
            TimesUtil timesUtil = new TimesUtil(TimesUtil.Type.Click);
            return timesUtil.run(startTime, endTime, dateType);
        }
        if ("hours".equals(dime)) {
            TimesUtil timesUtil = new TimesUtil(TimesUtil.Type.ClickHours);
            return timesUtil.run(startTime, endTime, dateType);
        }
        return JsonMessage.getString(100, "时间维度不正确");
    }

    @RequestMapping(value = "data_status.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String data_status(String dateType, String startTime, String endTime) throws ParseException {
        TimesUtil timesUtil = new TimesUtil(TimesUtil.Type.Status);
        return timesUtil.run(startTime, endTime, dateType);
    }
}
