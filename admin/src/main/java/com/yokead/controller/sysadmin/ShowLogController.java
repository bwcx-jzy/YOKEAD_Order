package com.yokead.controller.sysadmin;

import com.yokead.common.base.AdminBaseControl;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jiangzeyin on 2017/9/8.
 */
@Controller
@RequestMapping("sysadmin")
public class ShowLogController extends AdminBaseControl {

    @RequestMapping(value = "showlog.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index() {
        return "sysadmin/showlog";
    }
}
