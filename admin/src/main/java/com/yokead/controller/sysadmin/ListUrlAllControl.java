package com.yokead.controller.sysadmin;

import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.tools.AllUrl;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/11.
 */
@Controller
@RequestMapping("sysadmin")
public class ListUrlAllControl extends AdminBaseControl {

    @RequestMapping(value = "listAll.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String listAll() {
        JSONArray jsonArray;
        try {
            jsonArray = AllUrl.start(Config.Nginx.getNginxConfigPath());
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取失败", e);
            return JsonMessage.getString(100, "获取失败");
        }
        return JsonMessage.getString(200, "获取成功", jsonArray);
    }


}
