package com.yokead.controller.sysadmin;

import cn.jiangzeyin.StringUtil;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.rewrite.ConfService;
import com.yokead.service.rewrite.RewriteIndexService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/9.
 */
@Controller
@RequestMapping("sysadmin")
public class ConfControl extends AdminBaseControl {
    @Resource
    private ConfService confService;

    @Resource
    private RewriteIndexService rewriteIndexService;

    @RequestMapping(value = "conf_info.html", produces = MediaType.TEXT_HTML_VALUE)
    public String confInfo(String name, String parent) {
        name = convertFilePath(name);
        parent = convertFilePath(parent);
        if (StringUtil.isEmpty(name)) {
            setAttribute("tip", "打开失败");
            return "sysadmin/conf_info";
        }
        if (!name.endsWith(".conf")) {
            setAttribute("tip", "请正确使用,参数");
            return "sysadmin/conf_info";
        }
        File file = new File(rewriteIndexService.getConfFile(), parent + "/" + name);
        if (!file.exists()) {
            setAttribute("tip", "不存在该信息");
            return "sysadmin/conf_info";
        }
        try {
            JSONObject jsonObject = confService.getRewrites(file.getPath(), true, true);
            setAttribute("Info", jsonObject);
            setAttribute("parent", parent);
            setAttribute("name", name);
            return "sysadmin/conf_info";
        } catch (IOException e) {
            setAttribute("tip", "打开失败，请联系管理员");
            return "sysadmin/conf_info";
        }
    }

}
