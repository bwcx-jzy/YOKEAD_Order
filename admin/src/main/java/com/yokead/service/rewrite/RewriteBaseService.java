package com.yokead.service.rewrite;

import com.yokead.system.log.SystemLog;
import com.yokead.util.CmdUtil;

import java.io.IOException;

/**
 * Created by Administrator on 2017/5/11.
 */
public abstract class RewriteBaseService {

//    public String getConfPath() {
//        return Config.getNginxConfigPath();
//    }

    public void reload(String reuse) {
        try {
            String info = CmdUtil.cmdExec("nginx -s reload");
            SystemLog.LOG().info("因为：" + reuse + " 重启nginx：" + info);
        } catch (IOException e) {
//            SystemLog.ERROR().error("保存跳转", e);
            SystemLog.ERROR().error("因为：" + reuse + " 重启nginx 失败：" + e.getLocalizedMessage(), e);
        }
    }
}
