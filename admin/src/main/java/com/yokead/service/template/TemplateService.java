package com.yokead.service.template;

import cn.jiangzeyin.StringUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.util.JsonUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

/**
 * @author jiangzeyin
 * @date 2018/5/11
 */
@Service
public class TemplateService {

    public Document getTemplateDocument(String tag) throws IOException {
        if (StringUtil.isEmpty(tag)) {
            return null;
        }
        File file = Config.Admin.getPageTemplatePath();
        File config = new File(file, "config.json");
        if (!config.exists() && !config.isFile()) {
            throw new IllegalArgumentException("没有配置相关信息");
        }
        JSONArray jsonArray = (JSONArray) JsonUtil.readJson(config.getPath());
        String template_path = null;
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            String tag_ = jsonObject.getString("tag");
            if (tag_.equals(tag)) {
                template_path = jsonObject.getString("template_path");
                break;
            }
        }
        if (StringUtil.isEmpty(template_path)) {
            return null;
        }
        template_path = StringUtil.clearPath(StringUtil.convertNULL(template_path).replace("..", ""));
        File t = new File(file, template_path);
        if (!t.exists() && !t.isFile()) {
            throw new IllegalArgumentException("模板文件不存在或者异常");
        }
        return Jsoup.parse(t, "UTF-8");
    }

    public JSONArray getTemplateField(String tag) throws IOException {
        Document document = getTemplateDocument(tag);
        if (document == null) {
            return null;
        }
        Elements elements = document.select("[fieldtype]");
        JSONArray fieldArray = new JSONArray();
        for (Element element : elements) {
            String fieldtext = element.attr("fieldtext");
            String fieldtype = element.attr("fieldtype");
            if (StringUtil.isEmpty(fieldtype)) {
                continue;
            }
            if (StringUtil.isEmpty(fieldtext)) {
                continue;
            }
            String fieldindex = element.attr("fieldindex");
            int index = StringUtil.parseInt(fieldindex, -1);
            if (index <= -1) {
                continue;
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", fieldtype);
            jsonObject.put("text", fieldtext);
            jsonObject.put("index", index);
            fieldArray.add(jsonObject);
        }
        fieldArray.sort((o1, o2) -> {
            JSONObject jsonObject1 = (JSONObject) o1;
            JSONObject jsonObject2 = (JSONObject) o2;
            int index1 = jsonObject1.getIntValue("index");
            int index2 = jsonObject2.getIntValue("index");
            return Integer.compare(index2, index1);
        });
        return fieldArray;
    }
}
