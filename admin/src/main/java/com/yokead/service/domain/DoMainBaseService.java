package com.yokead.service.domain;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.util.CharsetUtil;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.system.log.SystemLog;

import java.io.*;
import java.util.HashMap;

/**
 * Created by jiangzeyin on 2017/5/11.
 */
public abstract class DoMainBaseService {
    //protected static String bootPath;

//    String getBootPath() {
//        return SystemBean.getInstance().getEnvironment().getProperty("boot.conf");
//    }


    private static final HashMap<String, Object> DEFAULT_MAP = new HashMap<>();

    private static void init() {
        DEFAULT_MAP.put("format", "json");
        File file = Config.DnsPodConfig.getDnsPodConfigFile();
        try {
            //.readToString(file);
            String json = FileUtil.readString(file, CharsetUtil.CHARSET_UTF_8);
            JSONObject jsonObject = JSONObject.parseObject(json);
            //"30576,8659221fa98286ada86336cc005a4b27"
            DEFAULT_MAP.put("login_token", jsonObject.getString("login_token"));
        } catch (IORuntimeException e) {
            e.printStackTrace();
        }
    }

    HashMap<String, Object> getDefault() {
        if (DEFAULT_MAP.size() <= 0) {
            init();
        }
        return clone(DEFAULT_MAP);
    }

    private static <T extends Serializable> T clone(T obj) {
        T clonedObj = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            oos.close();
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            clonedObj = (T) ois.readObject();
            ois.close();
        } catch (Exception e) {
            SystemLog.ERROR().error("复制对象错误", e);
        }
        return clonedObj;

    }
}
