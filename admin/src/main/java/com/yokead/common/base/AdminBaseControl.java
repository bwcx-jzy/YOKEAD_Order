package com.yokead.common.base;

import com.yokead.common.UserUtil;
import com.yokead.system.init.InitUserName;

import java.io.IOException;

/**
 * @author Administrator
 * date 2017/5/16
 */
public abstract class AdminBaseControl extends AbstractBaseControl {
    protected String userName;

    @Override
    public void resetInfo() {
        super.resetInfo();
        this.userName = InitUserName.getUserName();
    }

    protected boolean isManager() {
        try {
            return UserUtil.isManage(userName);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
