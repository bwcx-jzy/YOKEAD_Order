package com.yokead.common;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.util.JsonUtil;

import java.io.IOException;
import java.util.Iterator;

/**
 * @author jiangzeyin
 * @date 2017/5/9
 */
public class UserUtil {
    /**
     * 登录方法
     *
     * @param name 登录名
     * @param pwd  密码
     * @return true
     */
    public static boolean login(String name, String pwd) {
        if (StrUtil.isEmptyOrUndefined(name)) {
            return false;
        }
        if (name.endsWith("_del")) {
            return false;
        }
        String json = FileUtil.readString(Config.Admin.getUserConfigFile(), CharsetUtil.CHARSET_UTF_8);
        JSONArray jsonArray = JSON.parseArray(json);
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            String userName = jsonObject.getString("name");
            if (userName == null) {
                continue;
            }
            String userPwd = jsonObject.getString("pwd");
            if (userPwd == null) {
                continue;
            }
            if (userName.equals(name) && userPwd.equals(pwd)) {
                return true;
            }
        }
        JSONObject user = Config.Admin.getDefaultUser();
        String userName = user.getString("name");
        String userPwd = user.getString("pwd");
        return userName != null && userPwd != null && userName.equals(name) && userPwd.equals(pwd);
//return userName != null && userName.equals(name);
    }

    public static boolean check(String name) throws IOException {
        String json = FileUtil.readString(Config.Admin.getUserConfigFile(), CharsetUtil.CHARSET_UTF_8);
        JSONArray jsonArray = JSON.parseArray(json);
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            String userName = jsonObject.getString("name");
            if (userName == null) {
                continue;
            }
            if (userName.equals(name)) {
                return true;
            }
        }
        JSONObject user = Config.Admin.getDefaultUser();
        String userName = user.getString("name");
        return userName != null && userName.equals(name);
    }

    /**
     * @param name
     * @return
     * @throws IOException
     */
    public static boolean isManage(String name) throws IOException {
        String json = FileUtil.readString(Config.Admin.getUserConfigFile(), CharsetUtil.CHARSET_UTF_8);
        JSONArray jsonArray = JSON.parseArray(json);
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            String userName = jsonObject.getString("name");
            if (userName.equals(name) && jsonObject.getBooleanValue("manage")) {
                return true;
            }
        }
        JSONObject user = Config.Admin.getDefaultUser();
        String userName = user.getString("name");
        return userName != null && userName.equals(name);
    }

    /**
     * 是否为系统管理员
     *
     * @param name 名称
     * @return true
     */
    public static boolean isSystemManage(String name) {
        JSONObject user = Config.Admin.getDefaultUser();
        String userName = user.getString("name");
        return userName != null && userName.equals(name);
    }

    public static Iterator<Object> getIterator() throws IOException {
        String filePath = Config.Admin.getUserConfigFile();
        JSONArray jsonArray = (JSONArray) JsonUtil.readJson(filePath);
        return jsonArray.iterator();
    }
}
