package com.yokead.nginxlog.entity;

/**
 * nginx 日志记录表。
 * <p>
 * 原有的nginx日志表的数据太多了。重新建一个表。恢复广告系统的日志统计功能
 *
 * @author yangheng
 */
public class Nginx_Log_2019 {

    private Long id;

    /**
     * IP地址
     */
    private String ip;
    /**
     * X-Real-IP
     */
    private String xip;
    /**
     * 时间
     */
    private long time;
    /**
     * 请求类型。get PUST
     */
    private String type;

    /**
     * 访问的url
     */
    private String url;
    /**
     * 域名
     */
    private String domain;

    /**
     * 日志文件的路径
     */
    private String filePath;


    /**
     * 响应状态码
     */
    private Integer status;
    /**
     * referer
     */
    private String referer;
    /**
     * user-agent
     */
    private String uag;

    private Long createTime = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getXip() {
        return xip;
    }

    public void setXip(String xip) {
        this.xip = xip;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getUag() {
        return uag;
    }

    public void setUag(String uag) {
        this.uag = uag;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public static String getCreateTableSql() {
        return CREATE_TABLE_SQL;
    }

    /**
     * 建表sql
     * nginx
     */
    public static final String CREATE_TABLE_SQL =
            " CREATE TABLE `Nginx_Log_2019` ("
                    + " `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',"
                    + " `ip`      VARCHAR (200)   COMMENT 'ip地址',"
                    + " `xip`     VARCHAR (200)   COMMENT '真实ip',"
                    + " `time`    INT (11)        COMMENT '时间戳',"
                    + " `type`    CHAR (10)       COMMENT '请求类型',"
                    + " `url`     VARCHAR (2000)  COMMENT 'url',"
                    + " `domain`  VARCHAR (500)  COMMENT '域名',"
                    + " `status`  INT(11)         COMMENT '状态',"
                    + " `referer` text            COMMENT 'referer',"
                    + " `filePath` VARCHAR (1000)  COMMENT '日志文件的路径 存路径的md5',"
                    + " `uag`     VARCHAR (1000)  COMMENT 'uag',"
                    + " `createTime` INT (11)     COMMENT 'time',"

                    + " PRIMARY KEY ( id )"
                    + " ) COMMENT '广告日志分析';";
}



