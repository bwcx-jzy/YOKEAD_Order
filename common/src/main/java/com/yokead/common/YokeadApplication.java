package com.yokead.common;

import cn.jiangzeyin.common.ApplicationBuilder;
import cn.simplifydb.database.DbWriteService;
import cn.simplifydb.database.config.DataSourceConfig;
import cn.simplifydb.system.DbLog;
import com.yokead.system.log.SystemLog;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jiangzeyin
 * Created by jiangzeyin on 2017/1/10.
 */
public class YokeadApplication extends ApplicationBuilder {

    private String module;

    /**
     * 创建启动对象
     *
     * @param sources sources
     * @return Builder
     * @throws NoSuchFieldException   e
     * @throws IllegalAccessException e
     */
    public static YokeadApplication createBuilder(String module, Object... sources) throws Exception {
        return new YokeadApplication(module, sources);
    }

    private YokeadApplication(String module, Object... sources) throws Exception {
        super(sources);
        this.module = module;
        addApplicationEventLoad(SystemLog::init);
        addApplicationEventClient(event -> {
            if (event instanceof ApplicationReadyEvent) {
                DbLog.setDbLogInterface(new DbLog.DbLogInterface() {
                    @Override
                    public void info(Object object) {
                        if (object != null) {
                            SystemLog.LOG().info(object.toString());
                        }
                    }

                    @Override
                    public void error(String msg, Throwable throwable) {
                        SystemLog.ERROR().error(msg, throwable);
                    }

                    @Override
                    public void warn(Object msg) {
                        if (msg != null) {
                            SystemLog.LOG().info(msg.toString());
                        }
                    }

                    @Override
                    public void warn(String msg, Throwable t) {
                        SystemLog.ERROR().error(msg, t);
                    }
                });
                try {
                    DataSourceConfig.init(Config.DbConfig.getDbConfigPath());
                    DbWriteService.setWriteInterface(new DbWriteService.WriteInterface() {
                        @Override
                        public String getDatabaseName(Class aClass) {
                            return "core";
                        }

                        @Override
                        public String getTableName(Class<?> class1, boolean isIndex, String index, boolean isDatabaseName) {
                            return class1.getSimpleName();
                        }
                    });
                } catch (Exception e) {
                    SystemLog.ERROR().error("初始化数据库错误", e);
                }
            }
        });

    }

    @Override
    public ConfigurableApplicationContext run(String... args) {
        if (args == null || args.length <= 0) {
            throw new IllegalArgumentException("please add jsonConfig args");
        }
        String jsonConfig = args[0];
        try {
            Config.config(jsonConfig, module);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Map<String, Object> map = new HashMap<>(1);
        map.put("server.port", Config.getPort());
        properties(map);
        return super.run(args);
    }
}
