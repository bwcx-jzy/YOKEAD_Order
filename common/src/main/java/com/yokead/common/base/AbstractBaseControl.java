package com.yokead.common.base;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.controller.base.AbstractController;
import org.springframework.http.HttpHeaders;

/**
 * @author jiangzeyin
 * Created by jiangzeyin on 2017/1/12.
 */
public abstract class AbstractBaseControl extends AbstractController {

    protected String getUserAgent() {
        return getHeader(HttpHeaders.USER_AGENT);
    }

    protected String convertFilePath(String path) {
        return StringUtil.clearPath(StringUtil.convertNULL(path).replace("..", ""));
    }
}
