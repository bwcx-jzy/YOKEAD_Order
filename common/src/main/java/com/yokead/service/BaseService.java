package com.yokead.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.util.JsonUtil;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by jiangzeyin on 2017/5/17.
 */
public class BaseService {


//    /**
//     * 获取订单配置信息目录
//     *
//     * @return file
//     */
//    public File getCustomerFilePath() {
//        File file = ;// new File(, "customer");
//        return file;
//    }

    public File getCustomerInfoFile() throws IOException {
        File file = new File(Config.Order.getOrderConfigPath(), "info.conf");
        if (!file.exists()) {
            //.writeFile(file.getPath(), "[]");
            FileUtil.writeString("[]", file, CharsetUtil.CHARSET_UTF_8);
        }
        return file;
    }

//    protected File getCustomerFile() {
//        return new File(NginxBaseControl.getOrderPath(), "conf/customer/");
//    }

//
//    public File getOrderTypeFilePath() {
//        return new File(Config.getOrderTypePath(), "ordertype");
//    }

    public static File getOrderTypeConfigFile() {
        File type = new File(Config.Order.getOrderTypePath(), "type.conf");
        if (!type.exists() && !type.isFile())
            throw new IllegalArgumentException("订单type 配置文件异常");
        return type;
    }


    public static JSONArray getOrderColumnByType(String orderType) throws IOException {
        JSONArray orderTypeArray = (JSONArray) JsonUtil.readJson(getOrderTypeConfigFile().getPath());
        if (orderTypeArray == null) {
            throw new RuntimeException("没有订单配置信息");
        }
        JSONArray tableInfo = null;
        for (int i = orderTypeArray.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = orderTypeArray.getJSONObject(i);
            String name = jsonObject.getString("name");
            if (orderType.equals(name)) {
                tableInfo = jsonObject.getJSONArray("table");

                JSONObject defaultColumn = jsonObject.getJSONObject("defaultColumn");
                if (defaultColumn != null) {
                    if (tableInfo == null) {
                        tableInfo = new JSONArray();
                    }
                    for (Map.Entry<String, Object> item : defaultColumn.entrySet()) {
                        if ("id".equalsIgnoreCase(item.getKey())) {
                            continue;
                        }
                        if ("createTime".equalsIgnoreCase(item.getKey())) {
                            continue;
                        }
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("name", item.getKey());
                        jsonObject1.put("desc", item.getValue());
                        tableInfo.add(jsonObject1);
                    }
                }

                break;
            }
        }
        return tableInfo;
    }

//    public static String getOrderPath() {
//        return SystemBean.getInstance().getEnvironment().getProperty("order.conf");
//    }

//    protected String getOrderUserConfFile() {
//        return String.format("%s/conf/customer/info.conf", getOrderPath());
//    }
}
