package com.yokead.system.log;

/**
 * @author jiangzeyin
 * create 2017 02 17 11:06
 */
public enum LogType {
    SQL, SQL_ERROR,
    CONTROL, CONTROL_ERROR,
    REQUEST, REQUEST_ERROR,
    DEFAULT, ERROR
}
