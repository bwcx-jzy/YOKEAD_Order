package com.yokead.database;

import javax.sql.DataSource;

/**
 * @author jiangzeyin
 * Created by jiangzeyin on 2017/1/6.
 */
public class DatabaseContextHolder {

    public static DataSource getDataSource() {
        return cn.simplifydb.database.config.DatabaseContextHolder.getWriteDataSource("core");
    }
}
