package com.yokead.controller.visitors;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.OrderListBaseController;
import com.yokead.common.interceptor.NotLogin;
import com.yokead.service.AnalyzeService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by jiangzeyin on 2018/5/16.
 */
@Controller
@RequestMapping("visitors")
public class VisitorsAnalyzeController extends OrderListBaseController {

    @Resource
    private AnalyzeService analyzeService;

    @RequestMapping(value = "analyze.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @NotLogin
    public String list() {
        return "visitors/analyze";
    }


    @RequestMapping(value = "analyze_data.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @NotLogin
    public String analyze_data(String date, String product) {
        JSONObject Auth = (JSONObject) getSession().getAttribute("Visitors_User_Auth");
        if (Auth == null)
            return JsonMessage.getString(100, "没有权限");
        boolean analyze = Auth.getBooleanValue("analyze");
        if (!analyze)
            return JsonMessage.getString(100, "没有权限");
        if (StringUtil.isEmpty(product))
            return JsonMessage.getString(405, "请选择产品");
        // 验证session
        String sKey = StringUtil.convertNULL(getSessionAttribute("Visitors_User"));
        if (StringUtil.isEmpty(sKey))
            return JsonMessage.getString(100, "获取失败");
        String[] tempS = StringUtil.stringToArray(sKey, "@");
        if (tempS == null || tempS.length != 2)
            return JsonMessage.getString(100, "获取失败，错误");
        String tableName = getTableName(tempS[0], product);
        if (StringUtil.isEmpty(tableName))
            return JsonMessage.getString(405, "产品信息获取失败");
        Object dateInt = analyzeService.parseDate(date);
        if (dateInt instanceof String) {
            return (String) dateInt;
        }
        int[] date_ = (int[]) dateInt;
        JSONObject jsonObject = analyzeService.getData(tableName, date_);
        return JsonMessage.getString(200, "", jsonObject);
    }
}
