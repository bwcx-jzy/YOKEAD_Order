package com.yokead.controller.admin;

import cn.hutool.http.HttpUtil;
import cn.jiangzeyin.StringUtil;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.OrderBaseControl;
import com.yokead.service.order.ProductService;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by jiangzeyin on 2017/5/16.
 */
@Controller
public class IndexControl extends OrderBaseControl {

    @Resource
    private ProductService productService;

    @RequestMapping(value = {"index.html", ""}, method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index() throws IOException {
        setAttribute("userName", userName);
        setAttribute("array", productService.getUserAll(userName));
        setAttribute("doorI", getSession().getAttribute("door"));
        return "admin/index";
    }

    @RequestMapping(value = "copyList.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String copyList(String url) {
        HashMap<String, Object> hashMap = new HashMap<>(2);
        hashMap.put("url", url);
        JSONObject jsonObject;
        String url_ = Config.Web.getCopyApiUrl();
        if (StringUtil.isEmpty(url_)) {
            setAttribute("tip", "请配置接口");
            return "admin/copyList";
        }
        try {
            String json = HttpUtil.post(url_, hashMap);
            jsonObject = JSONObject.parseObject(json);
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
            setAttribute("tip", "获取异常");
            return "admin/copyList";
        }
        int code = jsonObject.getIntValue("code");
        if (code != 200) {
            setAttribute("tip", jsonObject.getString("msg"));
            return "admin/copyList";
        }
        setAttribute("array", jsonObject.getJSONArray("data"));
        return "admin/copyList";
    }
}
