package com.yokead.controller.admin.list;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.OrderListBaseController;
import com.yokead.service.order.OrderService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by jiangzeyin on 2017/5/17.
 */
@Controller
public class ListControl extends OrderListBaseController {
    @Resource
    private OrderService orderService;

    /**
     * @return
     */
    @RequestMapping(value = "list.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String list(String productName) {
        productName = convertFilePath(productName);
        JSONObject jsonOrder = orderService.getProductColumn(userName, productName);
        if (jsonOrder == null)
            return "order/list_data";
        setAttribute("columns", jsonOrder.getJSONArray("columns"));
        //request.setAttribute("name", name);
        setAttribute("product", productName);
        return "admin/list/list";
    }


    @RequestMapping(value = "list_data.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String listData(String product, String pageNo, String pageSize, String type, String date) {
        product = convertFilePath(product);
        int pageNoInt = StringUtil.parseInt(pageNo, 1);
        int pageSizeInt = StringUtil.parseInt(pageSize, 10);
        return orderService.doSelect(userName, product, pageNoInt, pageSizeInt, type, 0, date, null, null);
    }

    /**
     * 获取广告位信息
     *
     * @param product 产品
     * @return
     */
    @RequestMapping(value = "ad_type.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String getAd(String product) {
        product = convertFilePath(product);
        return orderService.getAdType(userName, product);
    }

    @RequestMapping(value = "ad_product.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String getProduct(String product) {
        product = convertFilePath(product);
        return orderService.getProduct(userName, product);
    }

    /**
     * 作废
     *
     * @param product
     * @param id
     * @return
     */
    @RequestMapping(value = "zf.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String listData(String product, String id, String ids) {
        product = convertFilePath(product);
        String tableName = getTableName(userName, product);
        if (StringUtil.isEmpty(id) && !StringUtil.isEmpty(ids)) {
            String tempIds = StringUtil.filterHTML(ids);
            if (!ids.equals(tempIds))
                return JsonMessage.getString(400, "信息不正确，请检查ids");
        }
        return orderService.zf(tableName, id, ids, 1);
    }
}
