package com.yokead.controller.admin;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.yokead.common.OrderBaseControl;
import com.yokead.service.UserService;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/17.
 */
@Controller
public class UpdatePwd extends OrderBaseControl {

    @Resource
    private UserService userService;

    @RequestMapping(value = "reset_pwd.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String login_ing(String oldPwd, String newPwd) {
        boolean flag;
        try {
            flag = userService.login(userName, oldPwd, false, null);
        } catch (IOException e) {
            SystemLog.ERROR().error("修改密码", e);
            return JsonMessage.getString(145, "修改密码异常");
        }
        if (!flag)
            return JsonMessage.getString(145, "旧密码输入错误");
        if (StringUtil.isEmpty(newPwd, 6, 20))
            return JsonMessage.getString(150, "请输入6-20位的新密码");
        try {
            flag = userService.updatePwd(userName, newPwd);
            if (flag) {
                getSession().invalidate();
                return JsonMessage.getString(200, "修改成功");
            }
            return JsonMessage.getString(145, "修改失败");
        } catch (IOException e) {
            SystemLog.ERROR().error("修改密码", e);
            return JsonMessage.getString(145, "修改密码异常!");
        }
    }
}
