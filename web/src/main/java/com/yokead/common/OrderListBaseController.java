package com.yokead.common;

import com.alibaba.fastjson.JSONObject;
import com.yokead.service.order.ProductService;
import com.yokead.system.log.SystemLog;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2018/5/16.
 */
public abstract class OrderListBaseController extends OrderBaseControl {
    @Resource
    private ProductService productService;


    protected String getTableName(String pName, String product) {
        JSONObject jsonObject;
        try {
            jsonObject = productService.getUserProductJsonObj(pName, product);
        } catch (IOException e) {
            SystemLog.ERROR().error("获取表名", e);
            return null;
        }
        if (jsonObject == null)
            return null;
        return jsonObject.getString("tableName");
    }
}
