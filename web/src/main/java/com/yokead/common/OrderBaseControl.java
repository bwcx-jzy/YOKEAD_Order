package com.yokead.common;

import com.yokead.common.base.AbstractBaseControl;
import com.yokead.common.interceptor.LoginInterceptor;

/**
 * Created by jiangzeyin on 2017/5/16.
 */
public abstract class OrderBaseControl extends AbstractBaseControl {
    protected String userName;

    @Override
    public void resetInfo() {
        super.resetInfo();
        this.userName = getSessionAttribute(LoginInterceptor.SESSION_NAME);
    }
}
